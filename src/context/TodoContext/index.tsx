import {
  PropsWithChildren,
  createContext,
  useContext,
  useEffect,
  useState
} from 'react'
import { TodoList } from '../../interfaces/TodoList'
import { v4 as uuid } from 'uuid'

type TodoContextType = {
  todoList: TodoList[]
  sendForm: React.FormEventHandler<HTMLFormElement>
  removeTask: (id: string) => void
}

const TodoContext = createContext<TodoContextType | undefined>(undefined)

export const useTodoContext = () => {
  const context = useContext(TodoContext)

  if (!context) {
    throw new Error('TodoContext no existe')
  }

  return context
}

const TodoProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const getElements = () => {
    const list = localStorage.getItem('todoListTest')
    if (list === null) return []
    return JSON.parse(list)
  }

  const [todoList, setTodoList] = useState<TodoList[]>(getElements())

  useEffect(() => {
    localStorage.setItem('todoListTest', JSON.stringify(todoList))
  }, [todoList])

  const sendForm: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault() // Evitar la recarga de la pagina

    const form = new FormData(e.target as HTMLFormElement) // Extracción de valores del formulario

    const task = form.get('task') as string
    const description = form.get('description') as string

    // Set information about task - almacenamiento de la tarea
    const newData = { id: uuid(), task, description }
    setTodoList((prev) => [...prev, newData])
  }

  const removeTask = (id: string) => {
    const filterTask = todoList.filter((task) => task.id !== id)
    setTodoList(filterTask)
  }

  const contextValue = {
    todoList,
    sendForm,
    removeTask
  }

  return (
    <TodoContext.Provider value={contextValue}>{children}</TodoContext.Provider>
  )
}

export default TodoProvider
