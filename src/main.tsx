import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './components/pages/App'
import { NextUIProvider } from '@nextui-org/react'
import './styles/global.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <NextUIProvider>
      <App />
    </NextUIProvider>
  </React.StrictMode>
)
