import { Divider } from '@nextui-org/react'
import TodoProvider from '../../../context/TodoContext'
import TodoForm from '../../organisms/TodoForm'
import TodoList from '../../organisms/TodoList'

const App = () => {
  return (
    <TodoProvider>
      <div className="container mx-auto max-w-screen-sm">
        <TodoForm />
        <Divider className="my-4" />
        <TodoList />
      </div>
    </TodoProvider>
  )
}

export default App
