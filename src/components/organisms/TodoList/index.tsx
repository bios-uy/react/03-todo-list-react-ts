import {
  Accordion,
  AccordionItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Divider
} from '@nextui-org/react'
import { useTodoContext } from '../../../context/TodoContext'
import { TodoList } from '../../../interfaces/TodoList'

const TodoList: React.FC = () => {
  const { todoList, removeTask } = useTodoContext()

  return (
    <Card>
      <CardHeader>
        <h1 className="text-2xl font-bold text-sky-950">Lista de tareas</h1>
      </CardHeader>
      <Divider />
      <CardBody>
        <div className="flex flex-col gap-y-4">
          {todoList.length === 0 && <h3>Agrega tareas a la lista</h3>}

          <Accordion>
            {todoList.map((item, key) => (
              <AccordionItem key={key} title={item.task}>
                <div className="flex justify-between items-center">
                  <p>{item.description}</p>
                  <Button onClick={() => removeTask(item.id)} color="danger">
                    Eliminar
                  </Button>
                </div>
              </AccordionItem>
            ))}
          </Accordion>
        </div>
      </CardBody>
    </Card>
  )
}

export default TodoList
