import {
  Input,
  Textarea,
  Button,
  Card,
  CardBody,
  CardHeader,
  Divider
} from '@nextui-org/react'
import { useTodoContext } from '../../../context/TodoContext'

const TodoForm: React.FC = () => {
  const { sendForm } = useTodoContext()

  return (
    <Card>
      <CardHeader>
        <h1 className="text-2xl font-bold text-sky-950">
          Formulario de tareas
        </h1>
      </CardHeader>
      <Divider />
      <CardBody>
        <form className="flex flex-col gap-y-4" onSubmit={sendForm}>
          <Input
            label="Titulo de la tarea"
            name="task"
            type="text"
            isRequired
          />

          <Textarea
            label="Descripción de la tarea"
            name="description"
            isRequired
          />

          <Button type="submit" color="primary">
            Crear Tarea
          </Button>
        </form>
      </CardBody>
    </Card>
  )
}

export default TodoForm
